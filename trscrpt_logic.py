'''
From Biological Networks course:
Different logic circuits on transcription regulation and visualisation with 3-D plot
'''


from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

def fun(A, B):
    return (A*B/0.01)/(1+(A*B/0.01)+A+B)
def OR(A,B):
    return (A*B/0.01+A+B)/(1+(A*B/0.01)+A+B)
def NAND(A,B):
    return 1/(1+(A*B/0.01))
def XOR(A,B):
    return (A+B)/(1+(A*B/0.01)+A+B)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.linspace(0.01, 10, 1000)
X, Y = np.meshgrid(x, y)

zs = np.array([XOR(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])
X = np.log2(X)
Y = np.log2(Y)

Z = zs.reshape(X.shape)
print(Z)

ax.plot_surface(X, Y, Z)
ax.set_zlim(0.01, 1.1)
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()
