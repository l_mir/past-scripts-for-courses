'''
From biological networks course:
implementation of the repressilator 
'''


import numpy as np
from scipy.integrate import ode
import matplotlib.pyplot as plt

if __name__ == "__main__":

    def f(t, y, arg1, arg2):
        y0 = np.real(y[0])
        y1 = np.real(y[1])
        y2 = np.real(y[2])
        y_prime_0 = arg2 + (1 / (1 + np.power(y2 / 0.2, arg1)) - y0)
        y_prime_1 = arg2 + (1 / (1 + np.power(y0 / 0.2, arg1)) - y1)
        y_prime_2 = arg2 + (1 / (1 + np.power(y1 / 0.2, arg1)) - y2)
        return np.array([[y_prime_0, y_prime_1, y_prime_2]], dtype = np.float64).reshape((3, 1))


    def jac(t, y, arg1):
        y0 = np.real(y[0])
        y1 = np.real(y[1])
        y2 = np.real(y[2])
        deriv_y0 = -(arg1 * np.power(y0, arg1 - 1)) / (
                np.power(0.2, arg1) * np.power(1 + np.power((y0 / 0.2), arg1), 2))
        deriv_y1 = -(arg1 * np.power(y1, arg1 - 1)) / (
                np.power(0.2, arg1) * np.power(1 + np.power((y1 / 0.2), arg1), 2))
        deriv_y2 = -(arg1 * np.power(y2, arg1 - 1)) / (
                np.power(0.2, arg1) * np.power(1 + np.power((y2 / 0.2), arg1), 2))
        jac = np.zeros(shape = (3, 3), dtype = np.float64)
        jac[0, 0] = -1
        jac[1, 1] = -1
        jac[2, 2] = -1
        jac[1, 0] = deriv_y0
        jac[2, 1] = deriv_y1
        jac[0, 2] = deriv_y2
        return jac

    r = ode(f, jac).set_integrator('zvode', method = 'bdf')
    y0, t0 = [0.5, 0.3, 0.1], 0
    h = 3.0
    epsilon = 0.05
    r.set_initial_value(y0, t0).set_f_params(h, epsilon).set_jac_params(h)
    t1 = 10
    dt = 0.005
    times = list()
    A = list()
    B = list()
    C = list()
    while r.successful() and r.t < t1:
        times.append(r.t + dt)
        values = r.integrate(r.t + dt)
        A.append(np.real(values[0]))
        B.append(np.real(values[1]))
        C.append(np.real(values[2]))
    plt.plot(times, A, 'r--', times, B, 'bs', times, C, 'g^')
    plt.show()