'''
Simulation of the sandpile_model:
https://en.wikipedia.org/wiki/Abelian_sandpile_model
'''

import numpy as np
import matplotlib.pyplot as plt
a = np.zeros((50,50))
r = 25
q = 25

def checkRest(mat):
    flattened = mat.flatten()
    for i in flattened:
        if flattened[i]>4:
            return False
        return True

a[r][q]=4
x=0
plt.axis([0,10,0,1])
plt.ion()
while (x<1000):  
    b = checkRest(a)  
    a[r][q] = a[r][q]+1
    
    for i in range(0, len(a)):
    
        for j in range(0, len(a[i])):
            if (i == 0 and a[i][j]>=4):
                if j==0:
                    a[i][j] = a[i][j] - 4
                    a[i+1][j] = a[i+1][j]+1
                    a[i][j+1] = a[i][j+1]+1

                else:
                    a[i][j] = a[i][j] - 4
                    a[i+1][j] = a[i+1][j]+1
                    a[i][j+1] = a[i][j+1]+1
                    a[i][j-1] = a[i][j-1]+1
                    a[i-1][j] = a[i-1][j]+1
                    
            if (i!=0 and a[i][j]>=4):
                if j==0:
                    a[i][j] = a[i][j] - 4
                    a[i+1][j] = a[i+1][j]+1
                    a[i][j+1] = a[i][j+1]+1
                    a[i-1][j] = a[i-1][j]+1
                if (j!=0 and i!=99):
                    a[i][j] = a[i][j] - 4
                    a[i+1][j] = a[i+1][j]+1
                    a[i][j+1] = a[i][j+1]+1
                    a[i][j-1] = a[i][j-1]+1
                    a[i-1][j] = a[i-1][j]+1
    x = x+1
    if (x>500 and b == True):
        vv = a.flatten()    
        

       # plt.hist(vv)
        #plt.show()      
    plt.pcolor(a)
    plt.draw()
    plt.pause(0.00001)

