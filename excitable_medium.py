'''
From Biological Networks course:
Simulation of spacially coupled NF-kB system as 'excitable medium'

'''


#%%

import numpy as np
import matplotlib.pyplot as plt

def isexcited():
    return True

m=np.zeros(100)
n=[False]*100
n[50]=True
sig = 5
x=0
matr=[]
while(x<200):
    for i in range(1, len(m)-1):
        if(m[i]<sig):
            m[i]=m[i]+1 
        if(m[i]>sig):
            m[i]==sig   
    for i in range(1, len(n)-1):
        if(n[i]==True):
            if(m[i-1]==sig):
                n[i-1]=True
        
            if(m[i+1]==sig):
                n[i+1]=True
            m[i]=0
            n[i]=False
    
    n[50]=True
    matr.append(list(n))
    x=x+1
    
    



init_T = np.zeros(100)
init_R = np.zeros(100)
init_T[50]=1

dt= 0.01
def calc_delT(T,R,prev,nex):
    p=20
    D=1.5

    return (p*(T**2))/(1+T**2) - (R*T)/(0.1+T)-T+D*(prev+nex-2*T)

def calc_delR(T,R):
    tau=5

    return T-R/tau 


def calc_T(delT,dt):
    return delT*dt 

def calc_R(delR,dt):
    return delR*dt 


t = 0
plot_mat = []
while(t<80):
    for i in range(1,len(init_T)-1):
        prev = init_T[i-1]
        nex = init_T[i+1]
        dT = calc_delT(init_T[i],init_R[i],prev,nex)
        dR = calc_delR(init_T[i],init_R[i])
        init_T[i] = init_T[i]+calc_T(dT,dt)
        init_R[i] = init_R[i]+calc_R(dR,dt)   
    init_T[50]=+1
    init_T[0]=0
    init_T[-1]=0
    plot_mat.append(list(init_T))
    t = t+dt

plt.matshow(np.transpose(plot_mat), fignum=100)
plt.gca().set_aspect('auto')
plt.show()


