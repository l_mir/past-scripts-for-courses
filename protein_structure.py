'''
Structural bioinformatics final exam:

Parsing and analyzing protein structures, 
and implementation of structural alignment algorithm

'''


import Bio.PDB as PDB
from Bio.PDB import *
from Bio.PDB.PDBParser import *
from Bio.PDB.Polypeptide import PPBuilder, three_to_one
import os
from os import listdir
import numpy as np
from numpy import *
from numpy.linalg import *
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from math import *
import random

files = listdir('top100H') #put file names in a list
atom_list= []               #initializing atom list
seq = []                #initializing sequence list
os.chdir('top100H')     #setting current directory to file directory 
atoms= []
for i in range(0, len(files)):      #iterating over the files
    try:                            #catching parsing related exceptions and passing them
        par = PDB.PDBParser(PERMISSIVE= 1, QUIET= True)
        structure = par.get_structure(i, files[i])  #parsing structures from each file
        
        ppb = CaPPBuilder(radius = 4.0)             #building polypeptide object based on distance criterion 4 angstrom   
        for pp in ppb.build_peptides(structure):    
            z = pp.get_sequence()                   #getting each sequence from each structure
                                              
            catoms = pp.get_ca_list()               #getting CA atoms from each structure
        atoms.append(catoms)                        #appending atoms to a list
        seq.append(z)                               #appending sequence to alist
                        
    except Exception, e:
        pass



coord_list = []

#getting coordinates from atoms
for c in range(0, len(atoms)):
    vv = atoms[c]
    temp_list = []
    for j in range(0, len(vv)):
        coord = vv[j].get_coord()
        temp_list.append(coord)
    coord_list.append(temp_list)



main_dict={} #initializing the dictionary that will contain 

# fill the main_dict in the first protein   
is_first_protein = True

for j in range(0, len(seq)):

    if is_first_protein:
        for i in range(0, len(seq[j])-5):
            str=seq[j][i:i+5]
            
            if str not in main_dict:
                main_dict[str]=[coord_list[j][i:i+5]]
        
        is_first_protein=False

    else:
        temp_dict={}
        
#first traversal find unique 5 sequences and make values as coordinates
        for i in range(0, len(seq[j])-5):
            str=seq[j][i:i+5]
            if str not in temp_dict:
                temp_dict[str]=[coord_list[j][i:i+5]]
#second traversal compare against main_dict and add the pairs  if same sequence is found in both, otherwise attach the new sequences to main_dict

        for key in temp_dict:  #adding the corresponding coordinates as keys
            
            if key in main_dict:
                main_dict[key].append(temp_dict[key])
            else:
                main_dict[key]=temp_dict[key]


for key, value in main_dict.items(): #delete items are found in 3 sequences
    if len(value) != 2 :
        del main_dict[key]

 #CODE TO GENERATE RANDOM 5-mers
main_dict={}


is_first_protein = True

for j in range(0, len(seq)):
    
    if is_first_protein:
        for i in range(0, len(seq[j])-5):
            i = random.randint(0, len(seq[j]))
            str=seq[j][i:i+5]
            
            if str not in main_dict:
                main_dict[str]=[coord_list[j][i:i+5]]
        
        is_first_protein=False

    else:
        temp_dict={}
        
#first traversal find unique 5 sequences and make values (counts) 1
        for i in range(0, len(seq[j])-5):
            i = random.randint(0, len(seq[j]))
            str=seq[j][i:i+5]
            if str not in temp_dict:
                temp_dict[str]=[coord_list[j][i:i+5]]
#second traversal compare against main_dict and increment the main value if same sequence is found in both, otherwise attach the new sequences to main_dict

        for key in temp_dict:
            
            if key in main_dict:
                main_dict[key].append(temp_dict[key])
            else:
                main_dict[key]=temp_dict[key]

for key, value in main_dict.items():
    if len(value) != 2 :
        del main_dict[key]
print main_dict

coord_pairs = main_dict.values()

for key, value in main_dict.items():
    if len(value) != 2 :
        del main_dict[key]




coord_pairs = main_dict.values()  
#print coord_pairs 
coord1 = []   #list of first coordinates
coord2 = []    
coord22 = []  #list of second coordinates
for i in range(0, len(coord_pairs)):
    coord1.append(coord_pairs[i][0])  
    coord2.append(coord_pairs[i][1])

for i in range(0, len(coord2)):  #since coord2 is a list of lists adding them to coord22
    coord22.append(coord2[i][0])


def rmsd(X,Y):
    centroidX = np.zeros((5,3))  #centroid of first (5,3) vector
    centroidY = np.zeros((5,3))  #centroid of second (5,3) vector
    centroidX = (np.sum(X))/len(X)  
    centroidY = (np.sum(Y))/len(Y)  
    newX = X - centroidX            #moving X to centre of mass
    newY = Y - centroidY            #moving Y to centre of mass
    transposeX = newX.T             #transpose X
    R = transposeX.dot(newY)         #Rotate Y by X
    v, s, wt = svd(R)               #singular value decomposition for correlation matrix
    z = diag([1,1,-1])              #making a diagonal matrix
    U = (wt.T) * (v.T)
    if np.linalg.det(U) == -1:      #checking for reflection
        wz= wt.T * z
        U = wz *(v.T)
    
    YU = newY.dot(U)                #rotating Y by U
    
    n = np.sum((newX - YU)*(newX-YU))     #calculating RMSD
    rmsd = sqrt(n)/sqrt(len(newX))
    return rmsd
rmsd_values = []    
for i in range(0, len(coord1)):      #calculating RMSD for all pairs
    
    rmsd_values.append(rmsd(coord1[i], coord22[i]))   
    
    
    
# histogram plot for RMSD values
mu, sigma = 0, 15

n, bins, patches = plt.hist(rmsd_values, 50, normed=2, facecolor='green', alpha=0.75)

plt.xlabel('RMSD values')
plt.ylabel('Probability')
plt.title(r'$\mathrm{Histogram\ of\ RMSD values:}\ \mu=0,\ \sigma=15$')
plt.axis([0, 200, 0, 0.03])
plt.grid(True)

plt.show() 

   
    
