#From Data Science course: Logistic regression implementation

from __future__ import division ##########
import numpy as np
import matplotlib.pyplot as plt

def get_data(data):
    '''Gets the data based on labels of interest 0,1.          
        Turns cm to mm for the second column                  
        Returns X_data and labels: Y_data'''
    #keep index of 0 and 1 labels
    idx=[]
    for i in range(len(data)):
        if data[i,2]==0 or data[i,2]==1:
            idx.append(i)
        
    # get as numpy.array the corresponding data
    list=[]
    for i in (idx):
        list.append(data[i,:])  
    data_array=np.stack(list)
    
    # Turn cm to mm for the second column
    for i in range(len(data_array)):
        data_array[i,1]=data_array[i,1] *10
    #measurements
    X_data=data_array[:,:2]
    #labels of 0 and 1
    Y_data=data_array[:,-1]  
    #turn labels of 0 and 1 -----> to -1 and 1
    Y_data = np.array((Y_data-.5)*2)
    return(X_data,Y_data)

# Training data
Train = np.loadtxt(r"C:\Users\Desktop\BINF\Assignment2\IrisTrainML.dt")
#features=X_train
#target= Y_train
features,target = get_data(Train)
features = np.hstack((np.ones((features.shape[0], 1)), features)) 
#################################################################################
def sigmoid(scores):
    '''Logistic function Theta '''
    return 1 / (1 + np.exp(-scores))     
#################################################################################
def logistic_insample(features, target, weights):
    ll=0
    N = features.shape[0]
    s = target*np.dot(features, weights)
    ll =  (1/N)* np.sum(np.log(1 + np.exp(-s)))
    return ll



#################################################################################
def logistic_gradient(features, target, weights):

    '''Returns the Gradient'''
    N = features.shape[0]
    g = 0*weights
    for n in range(N):                     
        g += (-1/N)*((target[n]*features[n,:])*sigmoid(-target[n]*np.dot(weights, features[n,:])))  ###(-1/N)*
    #np.sum(g)
    return g
################################################################################
def log_reg(features, target, max_iter, grad_thr,learningrate):              
    # INITialize  weights with zeros at t=0
    w = np.zeros(features.shape[1])
  
    # calculate log likelihood
    Ein= logistic_insample(features,target,w)
    # Keep track of the error , to check if it is decreasing as it should
    Elist = list()
    
    #keep track of the # of iterations needed
    num_iter = 0  
    convergence = 0
    
    # Keep track of the error , to check if it is decreasing as it should
    Elist = []
    
    while convergence == 0:
        num_iter = num_iter + 1                        
        # Compute gradient at current w      
        g = logistic_gradient(features,target,w)       
        # Set direction to move       
        v = -g                     
        # Update weights
        w_new = w + learningrate*v
        # Calculate new Ein
        Ein_new = logistic_insample(features,target,w_new)
        # Check if the in sample error is decreased
        if Ein_new < Ein:
            w = w_new
            Ein = Ein_new
            Elist.append(Ein)
            learningrate *=1.1
        else:
            learningrate *= 0.9   
        
        g_norm = np.linalg.norm(g)
        if g_norm < grad_thr:
            convergence = 1
        elif num_iter > max_iter:
            convergence = 1
           
    return w_new, Elist 

#################################################################################


# Get weights and the Erros of the of the training dataset
coef,E_list = log_reg(features,target, 5000, 0.000,0.1)



# Test data
Test = np.loadtxt(r"C:\Users\Desktop\BINF\Assignment2\IrisTestML.dt")
X_Test,Y_test = get_data(Test)
X_Test = np.hstack((np.ones((X_Test.shape[0], 1)), X_Test)) 


def log_predict(features, we):
    '''Given the coefficients and some data, make preditions'''
    N = features.shape[0]
    P = np.zeros(N)
    for n in range(N):
        P[n] = sigmoid(np.dot(we, features[n,:]))

        
    Pthresh = np.round(P) #0/1 class labels
    pred_classes = (Pthresh-0.5)*2 # make the -1 and +1 labels
    return pred_classes

# Predict the labels of the test data
test_preds=log_predict(X_Test,coef)

count_right_preds=0
for i in range(len(test_preds)):
    if test_preds[i] == Y_test[i]:
        count_right_preds+=1
#Testing Error 0-1 loss        
K = X_Test.shape[0]
Test_err=count_right_preds-K



#### plots
#cl0 = np.where(target== -1)
#cl1 = np.where(target==  1)
#data_matrix0 = features[cl0]
#data_matrix1 = features[cl1]
#plt.scatter(data_matrix0[:,1], data_matrix0[:,2], c='orange')
#plt.scatter(data_matrix1[:,1], data_matrix1[:,2], c='black')
#
#X = np.array([4,7])
#plt.plot(X, (-coef[0]-coef[1]*X)/coef[2])
#
#plt.title('Iris-Train-Set')
##plt.axis('equal')
#plt.show()
#plt.close()
#
#
#
#cl0 = np.where(Y_test== -1)
#cl1 = np.where(Y_test==  1)
#data_matrix0 = X_Test[cl0]
#data_matrix1 = X_Test[cl1]
#plt.scatter(data_matrix0[:,1], data_matrix0[:,2], c='orange')
#plt.scatter(data_matrix1[:,1],data_matrix1[:,2], c='black')
#
#X = np.array([4,7])
#plt.plot(X, (-coef[0]-coef[1]*X)/coef[2])
#
#plt.title('Iris-Test-Set')
##plt.axis('equal')
#plt.show()
#plt.close()

print 'In sample error Ein (or training error)=',E_list[-1]
print 'Training weights=',coef
print 'Testing Error=',Test_err