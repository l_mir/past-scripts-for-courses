'''
From Biological Networks course exercise:
Implementation of stochastic voter model on nucleosome changes
'''

#%%

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import random
random.seed()

def system_choice(ix,iy):
    x = abs(ix-iy)
    glpr=1/x
    lcpr=1-glpr
    
    return np.random.choice(['loc','glob'], p =[lcpr,glpr])

plot_mat = []
def ind(y):
    indices = [i for i, x in enumerate(list(nuc)) if x == y]
    return(indices)
nuc = np.random.randint(3, size=70)
plot_mat=[]
n=0
while(n<2500):
    i = np.random.choice(len(nuc))-1
    i2 = np.random.choice(len(nuc))-1
    if(i!=i2 and system_choice(i,i2)=='loc'):
        if(nuc[i-1]==1):
           nuc[i-1]=nuc[i]
        if(nuc[i+1]==1):
           nuc[i+1]=nuc[i]

    if(i!=i2 and system_choice(i,i2)=='glob'):
        if(nuc[i]==1):
            pass
        if(nuc[i]==0 and nuc[i2] !=0 and nuc[i2]!=1):
            nuc[i2]=nuc[i2]-1
        if(nuc[i]==2 and nuc[i2] !=2 and nuc[i2]!=1):
            nuc[i2]=nuc[i2]+1
    plot_mat.append(list(nuc))
    n=n+1

ax = sns.heatmap(plot_mat)    
plt.figure(figsize=(108,250))

plt.matshow(np.transpose(plot_mat))
plt.show()
                        

