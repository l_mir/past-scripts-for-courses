'''
Script from the CAKUT project that I completed during my bachelor's degree. 
First time I worked on python.

Processing .vcf files that were produced from processing exome data from patients.
The files contained the variants. This script is to compare the variants from different samples
and writing them to corresponding chromosome files. 
'''

import numpy as np
from matplotlib import pyplot as plt

#extracting chr#, position, ref and alt columns
def filestring (filename):   
   new_lines = []
   for line in filename:
       li=line.strip()
       if not li.startswith("#"):
           columns = line.split()
           row_line = columns[0] + ' ' + columns[1] + ' ' +  columns[3] + ' ' + columns[4]      
           new_lines.append(row_line)
   return new_lines 

filename1 = open('IV-5.vcf', 'r')
s = filestring(filename1) #list of IV-5 variants

filename2 = open('III-4.vcf', 'r')
m = filestring(filename2) #list of III-4 variants

filename3 = open('III-3.vcf', 'r')
d = filestring(filename3) #list of III-3 variants

filename4 = open ('IV-4.vcf', 'r')
c = filestring(filename4) #list of IV-4 variants

s_d = set(s).difference(d) 

m_s = set(m).intersection(s_d) 

c_m_s = set(c).intersection(m_s) 

def organizechr (str):
   chr = []
   for i in c_m_s:
       if i[:2]==str:
           chr.append(i)
   return chr




def chrToFile (chr, str, file_n):
    chr = organizechr (str)   
    with open(file_n, 'w') as f:
         data = f.write('\n'.join(chr)) 
   


chr1, chr2, chr3, chr4, chr5, chr6, chr7, chr8, chr9, chr10, chr11, chr12, chr13, chr14, chr15, chr16, chr17, chr18, chr19, chr20, chr21, chr22,  chrX, chrY=([] for i in range(24))

#saving the variants to files according to each chromosome
chrToFile(chr1, '1 ', 'chr1.txt')
chrToFile(chr2, '2 ', 'chr2.txt')
chrToFile(chr3, '3 ', 'chr3.txt')
chrToFile(chr4, '4 ', 'chr4.txt')
chrToFile(chr5, '5 ', 'chr5.txt')
chrToFile(chr6, '6 ', 'chr6.txt')
chrToFile(chr7, '7 ', 'chr7.txt')
chrToFile(chr8, '8 ', 'chr8.txt')
chrToFile(chr9, '9 ', 'chr9.txt')
chrToFile(chr10, '10', 'chr10.txt')
chrToFile(chr11, '11', 'chr11.txt')
chrToFile(chr12, '12', 'chr12.txt')
chrToFile(chr13, '13', 'chr13.txt')
chrToFile(chr14, '14', 'chr14.txt')
chrToFile(chr15, '15', 'chr15.txt')
chrToFile(chr16, '16', 'chr16.txt')
chrToFile(chr17, '17', 'chr17.txt')
chrToFile(chr18, '18', 'chr18.txt')
chrToFile(chr19, '19', 'chr19.txt')
chrToFile(chr20, '20', 'chr20.txt')
chrToFile(chr21, '21', 'chr21.txt')
