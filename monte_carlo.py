
'''
Monte carlo solution for 2-D Ising model

https://en.wikipedia.org/wiki/Square-lattice_Ising_model

'''

import numpy as np
import random
import math
import matplotlib.pyplot as plt
from numpy.oldnumeric.random_array import randint
J = 5
kbT = 10
h = 0

lat = np.random.randint(2, size = (20,20))
for i in range(0,20):
    for j in range(0,20):
        if lat[i][j]==0:
           lat[i][j]=-1 

def neighbors(m, x, y):
    return m[(x-1)%20][y]+m[(x+1)%20][y]+m[x][(y+1)%20]+m[x][(y-1)%20]


def ham(mat):
    E = 0
    for i in range(1,19):
        for j in range(1,19):
            p = neighbors(mat, i, j)
            E = E +(-2*mat[i][j]*p-h*mat[i][j])
    return E


def spinflip(m,x,y):
    m[x][y]*=-1
    return m   


def changeEn(m, flipped):
   # J = 0.0000000000003
   # kbT = 0.0000000005
    h = 0
   
    Ei=ham(m)
    
    Ef=ham(flipped)
    return -(Ef-Ei)

def acceptance(m):
    r = random.random()
    x = random.randint(1,19)
    y = random.randint(1,19)
    flipped = spinflip(m,x,y)
    tt= changeEn(m, flipped)

    if tt<=0:
        m=flipped
    if tt>0:
        y=math.exp(tt)
        if tt<r:
           m = m
        if tt>r:
           m = flipped   
    return m

n=0

def av_val(ar):
    H1 = []
    H2 = []
    for i in range(0, len(ar)):
        H1.append(ar[i]*math.exp(-ar[i]))
        H2.append(math.exp(-ar[i])) 
    return np.sum(H1)/np.sum(H2)
En = []
si = []
#for t in np.arange(0.1, 1000.1, 0.1):
while n<10000:
    lat = acceptance(lat)
    n=n+1
    plt.pcolor(lat)
    En.append(np.sum(lat))
    #si.append(t)
#plt.plot(si, En)
    plt.draw()
    plt.pause(1)
