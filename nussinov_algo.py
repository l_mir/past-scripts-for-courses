'''
implementation of Nussinov algorithm:
http://rna.informatik.uni-freiburg.de/Teaching/index.jsp?toolName=Nussinov

'''

DNAseq = 'CGAUUGCA'
n = len(DNAseq)
m=1
mat=[]
mat=[[0 for j in range(n)] for i in range(n)]

bp= {'AU', 'GC', 'GU', 'UA', 'CG', 'UG'}

for d in range(m+1, n):
    for i in range(0, n-d):
        j=i+d     
        if(DNAseq[i]+DNAseq[j] in bp):
            mat[i][j]=mat[i+1][j-1]+1
        elif(mat[i+1]>mat[i][j]):    
            mat[i][j]=mat[i+1][j]
        elif(mat[i]>mat[j-1]):
            mat[i][j]=mat[i][j-1]
            for k in range(i+1+m,j-1-m):
                if mat[i][k]+mat[k+1][j]>mat[i][j]:
                    mat[i][j]=mat[i][k]+mat[k+1][j ]
    print mat    
#print mat
